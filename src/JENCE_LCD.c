/****************************************************************************
 * Copyright (c) 2015 Lauris Radzevics                                      *
 *                                                                          *
 * Permission is hereby granted, free of charge, to any person obtaining    *
 * a copy of this software and associated documentation files (the          *
 * "Software"), to deal in the Software without restriction, including      *
 * without limitation the rights to use, copy, modify, merge, publish,      *
 * distribute, sublicense, and/or sell copies of the Software, and to       *
 * permit persons to whom the Software is furnished to do so, subject to    *
 * the following conditions:                                                *
 *                                                                          *
 * The above copyright notice and this permission notice shall be           *
 * included in all copies or substantial portions of the Software.          *
 *                                                                          *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE   *
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION    *
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          *
 ****************************************************************************/
 /**
 * @file JENCE_LCD.c
 * @author Lauris Radzevics
 * @date 09/06/2015
 * @brief LCD implementations for Jence library 
 *
 * This file contains LCD definitions for Jence library
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include "JENCE_globals.h"
#include "JENCE_GPIO.h"
#include "JENCE_LCD.h"

typedef struct
{
    int output[7];
} _DisplayDataMap;

_DisplayDataMap DDmap[2][16]=
{ 
  { {{0,0,0,0,0,0,0}}, {{0,0,0,0,0,0,1}}, {{0,0,0,0,0,1,0}}, {{0,0,0,0,0,1,1}}, {{0,0,0,0,1,0,0}}, {{0,0,0,0,1,0,1}}, {{0,0,0,0,1,1,0}}, {{0,0,0,0,1,1,1}}, {{0,0,0,1,0,0,0}}, {{0,0,0,1,0,0,1}}, {{0,0,0,1,0,1,0}}, {{0,0,0,1,0,1,1}}, {{0,0,0,1,1,0,0}}, {{0,0,0,1,1,0,1}}, {{0,0,0,1,1,1,0}}, {{0,0,0,1,1,1,1}} },
  { {{1,0,0,0,0,0,0}}, {{1,0,0,0,0,0,1}}, {{1,0,0,0,0,1,0}}, {{1,0,0,0,0,1,1}}, {{1,0,0,0,1,0,0}}, {{1,0,0,0,1,0,1}}, {{1,0,0,0,1,1,0}}, {{1,0,0,0,1,1,1}}, {{1,0,0,1,0,0,0}}, {{1,0,0,1,0,0,1}}, {{1,0,0,1,0,1,0}}, {{1,0,0,1,0,1,1}}, {{1,0,0,1,1,0,0}}, {{1,0,0,1,1,0,1}}, {{1,0,0,1,1,1,0}}, {{1,0,0,1,1,1,1}} }
};

JENCE_LCD_pins_t *G_LCD_pins;

void send_command(int rs, int e, int db7, int db6, int db5, int db4, int db3, int db2, int db1, int db0)
{
    JENCE_GPIO_set(&G_LCD_pins->rs, rs); /* RS  */
    JENCE_GPIO_set(&G_LCD_pins->e, e);  /* E   */
    JENCE_GPIO_set(&G_LCD_pins->db7, db7); /* DB7 */
    JENCE_GPIO_set(&G_LCD_pins->db6, db6); /* DB6 */
    JENCE_GPIO_set(&G_LCD_pins->db5, db5);  /* DB5 */
    JENCE_GPIO_set(&G_LCD_pins->db4, db4);  /* DB4 */
    JENCE_GPIO_set(&G_LCD_pins->db3, db3);  /* DB3 */
    JENCE_GPIO_set(&G_LCD_pins->db2, db2);  /* DB2 */
    JENCE_GPIO_set(&G_LCD_pins->db1, db1);  /* DB1 */
    JENCE_GPIO_set(&G_LCD_pins->db0, db0);  /* DB0 */
    usleep(5);
    JENCE_GPIO_set(&G_LCD_pins->e, JENCE_PIN_OFF); /* E   */
    JENCE_GPIO_set(&G_LCD_pins->rs, JENCE_PIN_OFF); /* RS  */
}

void init_display(void)
{
    /* wait 15 ms    */
    usleep(15000); 

    send_command(0, 1, 0, 0, 1, 1, 0, 0, 0, 0);
    usleep(4200);

    send_command(0, 1, 0, 0, 1, 1, 0, 0, 0, 0);
    usleep(110);
    
    send_command(0, 1, 0, 0, 1, 1, 0, 0, 0, 0);
    usleep(2000);
    
    send_command(0, 1, 0, 0, 1, 1, 1, 0, 0, 0);
    usleep(50);
    
    send_command(0, 1, 0, 0, 0, 0, 1, 0, 0, 0);
    usleep(50);
    
    send_command(0, 1, 0, 0, 0, 0, 0, 0, 0, 1);
    usleep(2000);
    
    send_command(0, 1, 0, 0, 0, 0, 0, 1, 1, 0);
    usleep(50);

    send_command(0, 1, 0, 0, 0, 0, 0, 0, 1, 0);
    usleep(2000);
    
    /* display on */
    send_command(0, 1, 0, 0, 0, 0, 1, 1, 1, 1);
    usleep(50);
}

void JENCE_LCD_clear_display(void)
{
    send_command(0, 1, 0, 0, 0, 0, 0, 0, 0, 1);
    usleep(2000);
}

void JENCE_LCD_cursor_home(void)
{
    send_command(0, 1, 0, 0, 0, 0, 0, 0, 1, 0);
    usleep(2000);
}

int JENCE_LCD_init(JENCE_LCD_pins_t *lcd_pins)
{
    JENCE_GPIO_init();
    
    G_LCD_pins = lcd_pins;
    
    JENCE_GPIO_config(&G_LCD_pins->rs, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->rw, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->e, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db0, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db1, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db2, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db3, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db4, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db5, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db6, JENCE_DIR_OUT);
    JENCE_GPIO_config(&G_LCD_pins->db7, JENCE_DIR_OUT);
    
    init_display();
    JENCE_LCD_clear_display();
    JENCE_LCD_cursor_home();
    return SUCCEED;
}

void JENCE_LCD_uninit(void)
{
    JENCE_GPIO_uninit();
}

void print_char_LCD(unsigned char chr)
{
    int output[8];
    int i;
    for (i = 0; i < 8; ++i) {
        output[i] = (chr >> i) & 1;
    }

    send_command(1, 1, output[7], output[6], output[5], output[4], output[3], 
                 output[2], output[1], output[0]);    
}

void JENCE_LCD_print(char *str)
{
    while(*str)
    {
        print_char_LCD(*str);
        str++;
    }
}

void JENCE_LCD_set_cursor(int line, int position)
{    
    send_command(0, 1, 1, DDmap[line-1][position-1].output[0], DDmap[line-1][position-1].output[1], 
                 DDmap[line-1][position-1].output[2], DDmap[line-1][position-1].output[3], 
                 DDmap[line-1][position-1].output[4], DDmap[line-1][position-1].output[5],
                 DDmap[line-1][position-1].output[6]);    
}
