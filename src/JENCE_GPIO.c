/****************************************************************************
 * Copyright (c) 2015 Lauris Radzevics                                      *
 *                                                                          *
 * Permission is hereby granted, free of charge, to any person obtaining    *
 * a copy of this software and associated documentation files (the          *
 * "Software"), to deal in the Software without restriction, including      *
 * without limitation the rights to use, copy, modify, merge, publish,      *
 * distribute, sublicense, and/or sell copies of the Software, and to       *
 * permit persons to whom the Software is furnished to do so, subject to    *
 * the following conditions:                                                *
 *                                                                          *
 * The above copyright notice and this permission notice shall be           *
 * included in all copies or substantial portions of the Software.          *
 *                                                                          *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE   *
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION    *
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          *
 ****************************************************************************/
 /**
 * @file JENCE_GPIO.c
 * @author Lauris Radzevics
 * @date 11/04/2015
 * @brief GPIO implementations of Jence library 
 *
 * This file contains GPIO definitions for Jence library
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <string.h>

#include "JENCE_GPIO.h"

/**
 * @var const signed int JENCE_GPIO_P8_PIN_SET[]
 * @brief GPIO pin group set of Beaglebone Black P8 header.
 * -1 as GND or VCC, Number 0/1/2 as GPIO 0/1/2
 */
const signed int JENCE_GPIO_P8_PIN_SET[] = 
    {-1, -1, 1, 1, 1, 1, 2, 2,
    2, 2, 1, 1, 0, 0, 1, 1, 
    0, 2, 0, 1, 1, 1, 1, 1, 
    1, 1, 2, 2, 2, 2, 0, 0, 
    0, 2, 0, 2, 2, 2, 2, 2, 
    2, 2, 2, 2, 2, 2};

/**
 * @var const unsigned int JENCE_GPIO_P8_PIN_OFFSET[]
 * @brief GPIO pin offsets of Beaglebone Black P8 header,
 * 0 as GND or VCC.
 */
const unsigned int JENCE_GPIO_P8_PIN_OFFSET[] = 
    {0,	0, 1<<6, 1<<7, 1<<2, 1<<3, 1<<2, 1<<3,	
    1<<5, 1<<4, 1<<13, 1<<12, 1<<23, 1<<26, 1<<15,	
    1<<14, 1<<27, 1<<1, 1<<22, 1<<31, 1<<30, 1<<5,	
    1<<4, 1<<1, 1<<0, 1<<29, 1<<22, 1<<24, 1<<23,	
    1<<25, 1<<10, 1<<11, 1<<9, 1<<17, 1<<8, 1<<16,	
    1<<14, 1<<15, 1<<12, 1<<13, 1<<10, 1<<11, 1<<8,	
    1<<9, 1<<6, 1<<7};

/**
 * @var const unsigned int JENCE_P8_PIN_ADDRESS[]
 * @brief GPIO pin addreses of Beaglebone Black P8 header,
 * 0 as GND or VCC.
 */				                                
const unsigned int JENCE_P8_PIN_ADDRESS[]= 
    { 0, 0, 0x818, 0x81C, 0x808, 0x80C, 0x890, 0x894,
    0x89C, 0x898, 0x834, 0x830, 0x824, 0x828, 0x83C, 0x838,
    0x82C, 0x88C, 0x820, 0x884, 0x880, 0x814, 0x810, 0x804, 
    0x800, 0x87C, 0x8E0, 0x8E8, 0x8E4, 0x8EC, 0x8D8, 0x8DC, 
    0x8D4, 0x8CC, 0x8D0, 0x8C8, 0x8C0, 0x8C4, 0x8B8, 0x8BC, 
    0x8B0, 0x8B4, 0x8A8, 0x8AC, 0x8A0, 0x8A4};

/**
 * @var const signed int JENCE_GPIO_P9_PIN_SET[]
 * @brief GPIO pin group set of Beaglebone Black P9 header.
 * -1 as GND or VCC, Number 0/1/2 as GPIO 0/1/2
 */
const signed int JENCE_GPIO_P9_PIN_SET[] = 
    {-1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, 0, 1, 0, 1, 1, 
    1, 0, 0, 0, 0, 0, 0, 1, 0, 
    3, 0, 3, 3, 3, 3, 3, -1, -1, 
    -1, -1, -1, -1, -1, -1, -1, 
    0, 0, -1, -1, -1, -1};

/**
 * @var const unsigned int JENCE_GPIO_P9_PIN_OFFSET[]
 * @brief GPIO pin offsets of Beaglebone Black P9 header,
 * 0 as GND or VCC.
 */
const unsigned int JENCE_GPIO_P9_PIN_OFFSET[]=
    {0, 0, 0, 0, 0, 0, 0, 0,	
    0, 0, 1<<30, 1<<28, 1<<31, 1<<18, 1<<16, 1<<19,	
    1<<5, 1<<4, 1<<13, 1<<12, 1<<3, 1<<2, 1<<17,	
    1<<15, 1<<21, 1<<14, 1<<19, 1<<17, 1<<15, 1<<16,	
    1<<14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1<<20,	
    1<<7, 0, 0, 0, 0};
/**
 * @var const unsigned int JENCE_P9_PIN_ADDRESS[]
 * @brief GPIO pin addreses of Beaglebone Black P9 header,
 * 0 as GND or VCC.
 */	
const unsigned int JENCE_P9_PIN_ADDRESS[]= 
    { 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0x870, 0x878, 0x874, 0x848, 0x840, 0x84C,
    0x95C, 0x958, 0x97C, 0x978, 0x954, 0x950, 0x844, 0x984, 
    0x9AC, 0x980, 0x9A4, 0x99C, 0x994, 0x998, 0x990, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 
    0x9b4, 0x964, 0, 0, 0, 0};

/**
 * @var int _fdmem
 * @brief variable for mapping /dev/mem memory
 */	
int _fdmem;
/**
 * @var volatile void *JENCE_gpio_addr[4]
 * @brief Array with mapped GPIO register memory
 */	
volatile void *JENCE_gpio_addr[4] = {NULL, NULL, NULL, NULL};
/**
 * @var signed int *JENCE_GPIO_PIN_SET[2]
 * @brief Array with GPIO pin group sets
 */	
signed int *JENCE_GPIO_PIN_SET[2];
/**
 * @var unsigned int *JENCE_GPIO_PIN_OFFSET[2]
 * @brief Array with GPIO pin offsets
 */	
unsigned int *JENCE_GPIO_PIN_OFFSET[2];
/**
 * @var unsigned int *JENCE_PIN_ADDRESS[2]
 * @brief Array with GPIO pin addreses
 */	
unsigned int *JENCE_PIN_ADDRESS[2];


void JENCE_GPIO_set_dir(JENCE_pin_t *pin, int dir)
{
    volatile unsigned int *gpio_oe_addr = NULL;
    
    gpio_oe_addr = JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]] + JENCE_GPIO_OE;
    
    if (dir == JENCE_DIR_OUT) 
    {
		*gpio_oe_addr &= ~(JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1]);
    }
    else if (dir == JENCE_DIR_IN) 
    {
            *gpio_oe_addr |= JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1];
    }
}

void JENCE_GPIO_set(JENCE_pin_t *pin, int status)
{
    volatile unsigned int *temp_addr = NULL;
    
    if(JENCE_PIN_ON == status)
    {
        temp_addr = JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]] + JENCE_GPIO_SETDATAOUT;
    }
    else if (JENCE_PIN_OFF == status)
    {
        temp_addr = JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]] + JENCE_GPIO_CLEARDATAOUT;
    }
    *temp_addr = JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1];
}

unsigned int JENCE_GPIO_read(JENCE_pin_t *pin)
{
    return (*((unsigned int *)(JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]]+JENCE_GPIO_DATAIN)) & JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1]);
}

int JENCE_GPIO_is_low(JENCE_pin_t *pin)
{
    return ((*((unsigned int *)(JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]]+JENCE_GPIO_DATAIN)) & JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1]) == 0);
}

int JENCE_GPIO_is_high(JENCE_pin_t *pin)
{
    return ((*((unsigned int *)(JENCE_gpio_addr[JENCE_GPIO_PIN_SET[pin->header][pin->pin-1]]+JENCE_GPIO_DATAIN)) & JENCE_GPIO_PIN_OFFSET[pin->header][pin->pin-1]) != 0);
}

void JENCE_GPIO_config(JENCE_pin_t *pin, int direction)
{
    /*To change PIN mode! */
    /*volatile unsigned int *gpio_conf_addr = NULL;
    unsigned int tmp;
    
    gpio_conf_addr = JENCE_gpio_addr[JENCE_GPIO_PIN_SET[header][pin-1]] + JENCE_PIN_ADDRESS[header][pin-1];

    tmp = *gpio_conf_addr;
    tmp |= 7;
    *gpio_conf_addr = tmp;*/
    
    JENCE_GPIO_set_dir(pin, direction);
}

int JENCE_GPIO_init(void)
{
    const char memDevice[] = "/dev/mem";
    
    /* open /dev/mem and error checking */
    _fdmem = open( memDevice, O_RDWR | O_SYNC );
    
    if (_fdmem < 0)
    {
        printf("Failed to open the /dev/mem !\n");
        return FAIL;
    }
    
    JENCE_gpio_addr[0] = mmap(0, JENCE_GPIO0_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, _fdmem, JENCE_GPIO0_ADDR);
    JENCE_gpio_addr[1] = mmap(0, JENCE_GPIO1_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, _fdmem, JENCE_GPIO1_ADDR);
    JENCE_gpio_addr[2] = mmap(0, JENCE_GPIO2_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, _fdmem, JENCE_GPIO2_ADDR);
    JENCE_gpio_addr[3] = mmap(0, JENCE_GPIO3_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, _fdmem, JENCE_GPIO3_ADDR);
    
    if(JENCE_gpio_addr[0] == MAP_FAILED
        || JENCE_gpio_addr[1] == MAP_FAILED
        || JENCE_gpio_addr[2] == MAP_FAILED
        || JENCE_gpio_addr[3] == MAP_FAILED) 
    {
        printf("Unable to map GPIO\n");
        return FAIL;
    }
    
    JENCE_GPIO_PIN_SET[0]=(signed int *)JENCE_GPIO_P8_PIN_SET;
    JENCE_GPIO_PIN_SET[1]=(signed int *)JENCE_GPIO_P9_PIN_SET;
    JENCE_GPIO_PIN_OFFSET[0]=(unsigned int *)JENCE_GPIO_P8_PIN_OFFSET;
    JENCE_GPIO_PIN_OFFSET[1]=(unsigned int *)JENCE_GPIO_P9_PIN_OFFSET;
    JENCE_PIN_ADDRESS[0]=(unsigned int *)JENCE_P8_PIN_ADDRESS;
    JENCE_PIN_ADDRESS[1]=(unsigned int *)JENCE_P9_PIN_ADDRESS;

    return SUCCEED;
}

void JENCE_GPIO_uninit(void)
{
    if (_fdmem!=0) {
        close(_fdmem);
    }
}