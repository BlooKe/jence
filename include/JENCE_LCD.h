/****************************************************************************
 * Copyright (c) 2015 Lauris Radzevics                                      *
 *                                                                          *
 * Permission is hereby granted, free of charge, to any person obtaining    *
 * a copy of this software and associated documentation files (the          *
 * "Software"), to deal in the Software without restriction, including      *
 * without limitation the rights to use, copy, modify, merge, publish,      *
 * distribute, sublicense, and/or sell copies of the Software, and to       *
 * permit persons to whom the Software is furnished to do so, subject to    *
 * the following conditions:                                                *
 *                                                                          *
 * The above copyright notice and this permission notice shall be           *
 * included in all copies or substantial portions of the Software.          *
 *                                                                          *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE   *
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION    *
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          *
 ****************************************************************************/
 /**
 * @file JENCE_LCD.h
 * @author Lauris Radzevics
 * @date 11/04/2015
 * @brief Definitions for Jence library for LCD implementations
 *
 * This header file contains all LCD definitions for Jence library
 */

#ifndef JENCE_LCD_H
#define	JENCE_LCD_H

#include "JENCE_globals.h"
#include "JENCE_GPIO.h"

typedef struct
{
    JENCE_pin_t rs;
    JENCE_pin_t rw;
    JENCE_pin_t e;
    JENCE_pin_t db0;
    JENCE_pin_t db1;
    JENCE_pin_t db2;
    JENCE_pin_t db3;
    JENCE_pin_t db4;
    JENCE_pin_t db5;
    JENCE_pin_t db6;
    JENCE_pin_t db7;
} JENCE_LCD_pins_t;

void JENCE_LCD_clear_display(void);
void JENCE_LCD_cursor_home(void);
int JENCE_LCD_init(JENCE_LCD_pins_t *lcd_pins);
void JENCE_LCD_uninit(void);
void JENCE_LCD_print(char *str);
void JENCE_LCD_set_cursor(int line, int position);

#endif	/* JENCE_LCD_H */

