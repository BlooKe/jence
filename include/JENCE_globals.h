/****************************************************************************
 * Copyright (c) 2015 Lauris Radzevics                                      *
 *                                                                          *
 * Permission is hereby granted, free of charge, to any person obtaining    *
 * a copy of this software and associated documentation files (the          *
 * "Software"), to deal in the Software without restriction, including      *
 * without limitation the rights to use, copy, modify, merge, publish,      *
 * distribute, sublicense, and/or sell copies of the Software, and to       *
 * permit persons to whom the Software is furnished to do so, subject to    *
 * the following conditions:                                                *
 *                                                                          *
 * The above copyright notice and this permission notice shall be           *
 * included in all copies or substantial portions of the Software.          *
 *                                                                          *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE   *
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION    *
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          *
 ****************************************************************************/
/**
 * @file JENCE_globals.h
 * @author Lauris Radzevics
 * @date 11/04/2015
 * @brief Definitions for Jence library for all common supported implementations
 *
 * This header file contains all common definitions of supported implementations 
 * in Jence library
 */
#ifndef JENCE_GLOBALS_H
#define JENCE_GLOBALS_H

/**
 * @def SUCCEED
 * @brief Definition for function's successful return value
 */
#define SUCCEED         0
/**
 * @def FAIL
 * @brief Definition for function's failed return value
 */
#define FAIL            (-1)
/**
 * @def JENCE_PIN_OFF
 * @brief Definition for pin turned Off (low value)
 */
#define JENCE_PIN_OFF   0
/**
 * @def JENCE_PIN_ON
 * @brief Definition for pin turned On (high value)
 */
#define JENCE_PIN_ON    1
/**
 * @def JENCE_DIR_OUT
 * @brief Definition for pin direction (Output) 
 */
#define JENCE_DIR_OUT   0
/**
 * @def JENCE_DIR_IN
 * @brief Definition for pin direction (Input)
 */
#define JENCE_DIR_IN    1
/**
 * @def JENCE_P8 
 * @brief Definition for BeagleBone Black P8 header
 */
#define JENCE_P8        0
/**
 * @def JENCE_P9
 * @brief Definition for BeagleBone Black P9 header 
 */
#define JENCE_P9        1

#endif