/****************************************************************************
 * Copyright (c) 2015 Lauris Radzevics                                      *
 *                                                                          *
 * Permission is hereby granted, free of charge, to any person obtaining    *
 * a copy of this software and associated documentation files (the          *
 * "Software"), to deal in the Software without restriction, including      *
 * without limitation the rights to use, copy, modify, merge, publish,      *
 * distribute, sublicense, and/or sell copies of the Software, and to       *
 * permit persons to whom the Software is furnished to do so, subject to    *
 * the following conditions:                                                *
 *                                                                          *
 * The above copyright notice and this permission notice shall be           *
 * included in all copies or substantial portions of the Software.          *
 *                                                                          *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE   *
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION    *
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          *
 ****************************************************************************/
 /**
 * @file JENCE_GPIO.h
 * @author Lauris Radzevics
 * @date 11/04/2015
 * @brief Definitions for Jence library for GPIO implementations
 *
 * This header file contains all GPIO definitions for Jence library
 */
#ifndef JENCE_GPIO_H
#define JENCE_GPIO_H

#include "JENCE_globals.h"

/**
 * @def JENCE_GPIO0_ADDR
 * @brief Start of BeagleBone Black GPIO0 register
 */
#define JENCE_GPIO0_ADDR     0x44E07000
/**
 * @def JENCE_GPIO0_ADDR_END
 * @brief End of BeagleBone Black GPIO0 register
 */
#define JENCE_GPIO0_ADDR_END     0x44E08FFF
/**
 * @def JENCE_GPIO0_SIZE
 * @brief Size of BeagleBone Black GPIO0 register
 */
#define JENCE_GPIO0_SIZE   (JENCE_GPIO0_ADDR_END - JENCE_GPIO0_ADDR )

/**
 * @def JENCE_GPIO1_ADDR
 * @brief Start of BeagleBone Black GPIO1 register
 */
#define JENCE_GPIO1_ADDR     0x4804C000
/**
 * @def JENCE_GPIO1_ADDR_END
 * @brief End of BeagleBone Black GPIO1 register
 */
#define JENCE_GPIO1_ADDR_END     0x4804DFFF
/**
 * @def JENCE_GPIO1_SIZE
 * @brief Size of BeagleBone Black GPIO1 register
 */
#define JENCE_GPIO1_SIZE   (JENCE_GPIO1_ADDR_END - JENCE_GPIO1_ADDR )

/**
 * @def JENCE_GPIO2_ADDR
 * @brief Start of BeagleBone Black GPIO2 register
 */
#define JENCE_GPIO2_ADDR     0x481AC000
/**
 * @def JENCE_GPIO2_ADDR_END
 * @brief End of BeagleBone Black GPIO2 register
 */
#define JENCE_GPIO2_ADDR_END     0x481ADFFF
/**
 * @def JENCE_GPIO2_SIZE
 * @brief Size of BeagleBone Black GPIO2 register
 */
#define JENCE_GPIO2_SIZE   (JENCE_GPIO2_ADDR_END - JENCE_GPIO2_ADDR )

/**
 * @def JENCE_GPIO3_ADDR
 * @brief Start of BeagleBone Black GPIO3 register
 */
#define JENCE_GPIO3_ADDR     0x481AE000
/**
 * @def JENCE_GPIO3_ADDR_END
 * @brief End of BeagleBone Black GPIO3 register
 */
#define JENCE_GPIO3_ADDR_END     0x481AFFFF
/**
 * @def JENCE_GPIO3_SIZE
 * @brief Size of BeagleBone Black GPIO3 register
 */
#define JENCE_GPIO3_SIZE   (JENCE_GPIO3_ADDR_END - JENCE_GPIO3_ADDR )
/**
 * @def JENCE_CONTROL_ADDR
 * @brief Start of the GPIO control address of BeagleBone Black
 */
#define JENCE_CONTROL_ADDR      0x44E10000
/**
 * @def JENCE_CONTROL_ADDR_END
 * @brief End of the GPIO control address of BeagleBone Black
 */
#define JENCE_CONTROL_ADDR_END  0x44E11FFF
/**
 * @def JENCE_CONTROL_SIZE
 * @brief Size of the GPIO control address of BeagleBone Black
 */
#define JENCE_CONTROL_SIZE      (JENCE_CONTROL_ADDR_END - JENCE_CONTROL_ADDR )
/**
 * @def JENCE_GPIO_OE
 * @brief BeagleBone Black GPIO OE offset
 */
#define JENCE_GPIO_OE           0x134
/**
 * @def JENCE_GPIO_SETDATAOUT
 * @brief BeagleBone Black GPIO offset for setting high value register
 */
#define JENCE_GPIO_SETDATAOUT   0x194
/**
 * @def JENCE_GPIO_CLEARDATAOUT
 * @brief BeagleBone Black GPIO offset for setting low value register
 */
#define JENCE_GPIO_CLEARDATAOUT 0x190
/**
 * @def JENCE_GPIO_DATAIN
 * @brief BeagleBone Black GPIO offset for incoming data register
 */
#define JENCE_GPIO_DATAIN       0x138
/**
 * @def JENCE_GPIO_DATAOUT
 * @brief BeagleBone Black GPIO offset for outgoing data register
 */
#define JENCE_GPIO_DATAOUT      0x13C

typedef struct
{
    int header;
    int pin;
} JENCE_pin_t;

/**
 * @brief Jence GPIO initialization function
 * @return SUCCEED or FAIL
 * 
 * Example:
 * @code
 * if(SUCCEED != JENCE_GPIO_init())
 * {
 * ret = FAIL;
 * }
 * else
 * {
 *    do something
 * }
 * @endcode
 */
int JENCE_GPIO_init(void);
/**
 * @brief Jence GPIO uninitialization function
 * @return void
 * 
 * Example:
 * @code
 * JENCE_GPIO_uninit();
 * @endcode
 */
void JENCE_GPIO_uninit(void);
/**
 * @brief Set pin On or Off
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @param [in] status Necessary status of pin (JENCE_PIN_ON or JENCE_PIN_OFF)
 * @return void
 * 
 * Example:
 * @code
 * JENCE_GPIO_SET(JENCE_P8, 14, JENCE_PIN_ON);
 * @endcode
 */
void JENCE_GPIO_set(JENCE_pin_t *pin, int status);
/**
 * @brief Configure Pin 
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @param [in] direction Direction of pin (JENCE_DIR_IN or JENCE_DIR_OUT)
 * @return void
 * 
 * Example:
 * @code
 * JENCE_GPIO_config(JENCE_P8, 14, JENCE_DIR_OUT);
 * JENCE_GPIO_config(JENCE_P8, 15, JENCE_DIR_IN);
 * @endcode
 */
void JENCE_GPIO_config(JENCE_pin_t *pin, int direction);
/**
 * @brief Set direction of pin 
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @param [in] dir Direction of pin (JENCE_DIR_IN or JENCE_DIR_OUT)
 * @return void
 * 
 * Example:
 * @code
 * JENCE_GPIO_set_dir(JENCE_P8, 14, JENCE_DIR_OUT);
 * JENCE_GPIO_set_dir(JENCE_P8, 15, JENCE_DIR_IN);
 * @endcode
 */
void JENCE_GPIO_set_dir(JENCE_pin_t *pin, int dir);
/**
 * @brief Read input GPIO pin value 
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @return Unsigned integer of incoming value register
 * 
 * Example:
 * @code
 * unsigned int i = JENCE_GPIO_READ(JENCE_P8, 15);
 * @endcode
 */
unsigned int JENCE_GPIO_read(JENCE_pin_t *pin);
/**
 * @brief Return if input GPIO pin has low value 
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @return integer 0 (FALSE) or 1 (TRUE)
 * 
 * Example:
 * @code
 * if(JENCE_GPIO_is_low(JENCE_P8, 15))
 * {
 *     Do something
 * }
 * @endcode
 */
int JENCE_GPIO_is_low(JENCE_pin_t *pin);
/**
 * @brief Return if input GPIO pin has high value 
 * @param [in] header Header of BeagleBone Black (JENCE_P8 or JENCE_P9)
 * @param [in] pin Pin number in header
 * @return integer 0 (FALSE) or 1 (TRUE)
 * 
 * Example:
 * @code
 * if(JENCE_GPIO_is_high(JENCE_P8, 15))
 * {
 *    Do something
 * }
 * @endcode
 */
int JENCE_GPIO_is_high(JENCE_pin_t *pin);

#endif